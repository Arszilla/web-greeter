web-greeter for Debian
----------------------

This package contains a community fork of Antergos' web-greeter, which may be
better known as lightdm-webkit2-greeter. web-greeter allows its users to
customize LightDM with HTML, CSS, and JS-based themes, allowing modern and
visually appealing LightDM themes.

This package builds and installs the necessary binaries, etc., to allow its
users to use web-greeter. To use web-greeter, uncomment greeter-session= found
under [Seat:*] (/etc/lightdm/lightdm.conf) and update its value to
web-greeter. To modify your web-greeter, edit /etc/lightdm/web-greeter.yml to
your heart's content.

Due to the use of git submodules in the original (community) repository, the
tarball had to be packed in an "unconventional" manner - the repository has to
be cloned using "git clone --rescursive" and then have the ".git/" folder and
".git" file located under themes/ and themes/ts-types deleted before being
repackaged with "tar -czf web-greeter_VERSION.orig.tar.gz web-greeter/". Only
after this is done, the tarball can be imported using "gbp import-orig", thus
allowing the package to be built without any problems with
"gbp buildpackage --git-builder=sbuild".

 -- Arslan Masood <contact@arszilla.com>  Tue, 05 Mar 2024 22:45:00 +0200
